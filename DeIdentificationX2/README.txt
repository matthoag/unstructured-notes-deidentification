﻿De-identification User Guide

Jay Urbain, CTI/MCW
9/15/2014, 1/16/2015

Project: DeIdentificationX2

Description:
Automated de-identification of protected health information from electronic health records.

Input:  SQL query providing record text to de-identify.
Output: A newly created relational database table containing de-identified records.

Processing flow. Relevant classes are show within (): 
1) Read records to de-identify (deidentification.Deidentification)
2) Blacklist and whitelist processing (deidentificationRegex.compositeRegex) 
3) Pre-processing and regular expression processing. MCW implementation 
(deidentification.mcw.DeidentificationRegexMCW) removes invalid character encodings,
places spaces between mixed capitalization terms and mixed alpha numerics; and
de-id's dates, MRN/ids, phone, email, and addresses.
jUnit tests have been written for regression testing of regular expressions 
(deidentification.mcw.DeidentificationRegexMCWTest).
4) Named entity recognition for identification and de-id of person and location entity types. MCW implementation
(deidentification.mcw.NamedEntityRecognitionMCW) uses 3 Stanford NLP named entity models trained 
are different text repositories. Entiites are replaced with [XXXXX]
5) Output original record text, regex record text, fully de-id'd text, and time-based performance
measurements to output table.

Main class: deidentification.DeIdentification

Program arguments (all arguments are required):
-dburl    – Database URL
-login    – Database login name
-password – Database password
-dbname   – Database name
-dbdriver – Database driver. Note: only MySQL has been validated. E.g., "com.mysql.jdbc.Driver"
-nthreads – Number of concurrent threads for processing records.
-recordsperthread 
          – Number of records assigned to each thread.
-query    –  Input query of records to de-identify. Select must be of the following form: 
             "select  id, note_id, note_text, date_off”  
		     where:
             id – unique identifier
             note_id – record note id. Does not have to be distinct. E.g., in EPIC, notes can be split.
             note_text – text to be de-identified.
             date_off – numeric offset for date de-identification, e.g., -15 t0 +15. Set to zero to not de-id dates.
-deidnotestablename 
          – name of database output table. The program will automatically create this table.
-whitelistfilename 
          – text file containing terms to NOT de-identify. Phrases are currently tokenized and treated as 
		  individual words. E.g., “Saranofsky,” is interpreted as a name, but it is also a common medical 
		  procedure. Whitelist terms are capitalized and pre- and post-fixed with '_'.
-blacklistfilename 
          - text file containing terms to ALWAYS de-identify. Phrases are currently tokenized and treated 
		  as individual words. Blacklist
		  terms are replaced with [XXXXX] in the output text.
-namedentityrecognitionclass 
          - Named entity class. Must implement the "deidentification.NamedEntityRecognition" abstract class.
-regexdeidentificationclass 
          - Regular expression class. Must implement the "deidentification.DeidentificationRegex" interface.

Program arguments with sample values:

-dburl "jdbc:mysql://proto1.ctsi.mcw.edu:3306"
-login "jurbain"
-password "xxxxx"
-dbname "nlp_jurbain"
-dbdriver "com.mysql.jdbc.Driver"
-nthreads 5
-recordsperthread 100
-query "select ID as id, NOTE_ID as note_id, NOTE_TEXT as note_text, DATE_OFF as date_off  
from JAY_HNO_NOTE_TEXT_COMB_RANDOM order by id"
-deidnotestablename "JAY_HNO_NOTE_TEXT_COMB_RANDOM_09122014"
-whitelistfilename "whitelist.txt"
-blacklistfilename "blacklist.txt"
-namedentityrecognitionclass "deidentification.mcw.NamedEntityRecognitionMCW"
-regexdeidentificationclass "deidentification.mcw.DeidentificationRegexMCW"

Minimum recommend JVM arguments:
-Xms4096M -Xmx4096M

Repository is laid out as an Eclipse project.

Prerequisites
 
Maven
Java JDK 1.7+
a Database ( Mysql , Postgres, Oracle ) 


Prep process for using this code 
  

- Checkout code from bitbucket with : 

    git clone https://userid@bitbucket.org/MCW_BMI/unstructured-notes-deidentification.git

    for the sake of documentation lets assume you've checked it out to the unstructured-notes-deidentification directory 

- Next populate your local Maven instance with Proprietary jars : 

    ./prep_mvn_repo.sh

-  Build jar file with 
    cd ~unstructured-notes-deidentification/DeIdentificationX2/
    mvn package

    This will generate ~unstructured-notes-deidentification/DeIdentificationX2/target/DeIdentificationX2-0.0.1-SNAPSHOT.one-jar.jar

- Customize the database connection parameters that are stored in the properties file

    cd ~unstructured-notes-deidentification/DeIdentificationX2/
    /runDid.sh

