/**
 * 
 */
package deidentification.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import deidentification.MedicalRecordWrapper;

/**
 * @author jayurbain
 * @author Matt Hoag
 *
 */
public class MySQLDBLocalization implements DBLocalization {

	final static String PRODUCT_NAME = "MySQL"; //Boo.  Hard coded value in mysql connector driver

	@Override
	public void createDeidNotesTable(Connection connection, String tableName) throws SQLException {
		
		Statement statement = connection.createStatement();

		String query1 = "DROP TABLE IF EXISTS " + tableName;
		statement.executeUpdate(query1);

		String query2 =
				"CREATE TABLE " + tableName + " ( " +
						"id INT(11) NOT NULL AUTO_INCREMENT, " +
						"note_id VARCHAR(254) DEFAULT NULL, " +
						"orig_note_text LONGTEXT DEFAULT NULL, " +
						"regex_note_text LONGTEXT DEFAULT NULL, " +
						"deid_note_text LONGTEXT DEFAULT NULL, " +
						"msecs_ner BIGINT DEFAULT 0, " +
						"msecs_regex BIGINT DEFAULT 0, " +
						"PRIMARY KEY (id) " +
						") ENGINE=MyISAM";
		statement.executeUpdate(query2);
	}

	@Override
	public String getLocalizationID()
	{
		return PRODUCT_NAME;
	}
}