/**
 *
 */
package deidentification.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 * @author jayurbain
 * @author Matt Hoag
 *
 */
public class PostgresDBLocalization implements DBLocalization {

    // Must match that returned from connection.getMetaData().getDatabaseProductName(); !
    public final static String PRODUCT_NAME = "PostgreSQL";
    private static final Logger LOG = Logger.getLogger(PostgresDBLocalization.class.getName());

    public PostgresDBLocalization() {
    }

    @Override
    public void createDeidNotesTable(Connection connection, String tableName) throws SQLException {

        Statement statement = null;
        statement = connection.createStatement();

        String query1 = "DROP TABLE IF EXISTS " + tableName;
        LOG.info("Removing  table : " + query1);
        statement.executeUpdate(query1);

        String query2
                = "CREATE TABLE " + tableName + " ( "
                + "id text NOT NULL, "
                + "note_id text DEFAULT NULL, "
                + "orig_note_text text DEFAULT NULL, "
                + "regex_note_text text DEFAULT NULL, "
                + "deid_note_text text DEFAULT NULL, "
                + "msecs_ner BIGINT DEFAULT 0, "
                + "msecs_regex BIGINT DEFAULT 0 "
                + "  )";
        LOG.info("Creating table : " + query2);
        statement.executeUpdate(query2);
    }

    @Override
    public String getLocalizationID() {
        return PRODUCT_NAME;
    }
}
