/**
 * @author Jay Urbain
 *
 * MCW Deidentification - Regular expressions for email, web url's, addresses,
 * dates, phone numbers, dates, MRN - Stanford NLP named entity recognition
 * models for person identification using CRF (Conditional Random Field)
 * 2/14/2014
 */
package deidentification;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;

import deidentification.db.DBConnection;

import deidentification.options.DeidOptions;
import deidentification.options.cmdr.DeidOptionsJCmd;
import deidentification.options.DeidOptionsParser;
import util.SimpleIRUtilities;

/**
 * @author jayurbain
 * @author Matt Hoag
 */
public class DeIdentification {

    /**
     * @param args
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void main(String[] args) {

        final DeidOptionsParser parser = new DeidOptionsJCmd();
        DeidOptions opts = null;

        try {
            opts = parser.parseOptions(args);
        } catch (DeidOptionsParser.InvalidOptions e) {
            System.out.println(e.getHelpText());
            if (e.getCause() != null) {
                System.out.println(e.getCause().getMessage());
                e.getCause().printStackTrace();
            }
            System.exit(e.getExitCode());
        }
        System.out.println("Updateable = " + opts.isUpdateOnly());
        String[] whiteListArray = null;
        String[] blackListArray = null;

        //////////////////////////////////////////////////
        // read white list - pass through list
        try {
            whiteListArray = loadFileList(opts.getWhitelistfile());
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        // read black list - block list
        try {
            blackListArray = loadFileList(opts.getBlacklistfile());
        } catch (IOException e2) {
            e2.printStackTrace();
        }

        Map<String, String> whiteListMap = new HashMap<String, String>();
        Map<String, String> blackListMap = new HashMap<String, String>();

        if (whiteListArray != null && whiteListArray.length > 0) {
            for (int i = 0; i < whiteListArray.length; i++) {
                String[] stringArray = whiteListArray[i].split("\\s+");
                for (int j = 0; j < stringArray.length; j++) {
                    String s = stringArray[j].toLowerCase();
                    whiteListMap.put(s, s);
                }
            }
        }
        if (blackListArray != null && blackListArray.length > 0) {
            for (int i = 0; i < blackListArray.length; i++) {
                String[] stringArray = blackListArray[i].split("[\\r\\n]+");
                for (int j = 0; j < stringArray.length; j++) {
                    String s = stringArray[j].toLowerCase();
                    blackListMap.put(s, s);
                }
            }
        }

        NamedEntityRecognition namedEntityRecognition = null;
        DeidentificationRegex deidentificationRegex = null;
        try {
            namedEntityRecognition = (NamedEntityRecognition) opts.getNamedentityrecognitionclass().newInstance();
            System.out.println("CLASS TO DEID WITH : " + opts.getRegexdeidentificationclass());
            deidentificationRegex = (DeidentificationRegex) opts.getRegexdeidentificationclass().newInstance();
        } catch (InstantiationException | IllegalAccessException e1) {
            System.out.println("Exception Caught ");
            e1.printStackTrace();
            System.exit(-1);
        }
        namedEntityRecognition.setWhiteListMap(whiteListMap);
        namedEntityRecognition.setBlackListMap(blackListMap);

        ///////////////////////////////////////////////////////////
        java.util.Date startDate = new java.util.Date();

        // load driver, get connection
        final Properties dbProps = new Properties();
        dbProps.put("user", opts.getLogin());
        dbProps.put("password", opts.getPassword());

        DBConnection connection;

        try {
            connection = new DBConnection(opts, dbProps);
        } catch (Exception e) {
            System.out.println("1: Could not connect to the database dburl: " + opts.getDburl()
                    + ", login: " + opts.getLogin() + ", password: " + opts.getPassword());
            e.printStackTrace();
            return;
        }

        // create output deid notes table
        if (!opts.isUpdateOnly()) {
            try {
                connection.createDeidNotesTable(opts.getDeidnotestablename());
            } catch (SQLException e1) {
                e1.printStackTrace();
                return;
            }
        }

        ////////////////////////////////////////////////////////
        // read database records with supplied query
        List<DeIdentificationThread> threadList = new ArrayList<DeIdentificationThread>();

        try {

            ResultSet results = connection.runQuery(opts.getQuery());
            DeIdentificationThread newT = new DeIdentificationThread(namedEntityRecognition);
            ResultSetMetaData rsMetaData = results.getMetaData();
            int columnCount = rsMetaData.getColumnCount();
            boolean patNameProvided = false;
            if (columnCount >= 5) { // assume pat_name provided
                patNameProvided = true;
            }

            System.out.println("***");
            while (results.next()) {
                String id = results.getString(1);
                String note_id = results.getString(2);
                //Supply an empty string instead of null if the Oracle value is "NULL"
                String text = results.getString(3) != null ? results.getString(3) : "";
                int dateOffset = results.getInt(4);
                String patientName = "";
                if (patNameProvided) {
                    patientName = results.getString(5);
                }
                System.out.print(id + " ");

                java.util.Date startDateRegex = new java.util.Date();
                String preprocessedText = deidentificationRegex.compositeRegex(text, dateOffset, blackListMap, patientName);
                java.util.Date endDateRegex = new java.util.Date();
                MedicalRecordWrapper record = new MedicalRecordWrapper(id, note_id, text, preprocessedText, null, dateOffset, patientName);
                long msecs = SimpleIRUtilities.getElapsedTimeMilliseconds(startDateRegex, endDateRegex);
                record.setMillisecondsRegex(msecs);

                newT.getRecordList().add(record);

                if (newT.getRecordList().size() >= opts.getRecordsPerThread()) {
                    System.out.println();
                    if (threadList.size() >= opts.getNthreads()) {
                        DeIdentificationThread t = threadList.remove(0);
                        try {
                            t.join();
                            if (opts.isUpdateOnly()) {
                                connection.updateDeidRecord(opts.getUpdateQuery(), t.getRecordList());
                            } else {
                                connection.insertDeidRecord(opts.getDeidnotestablename(), t.getRecordList());
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            return;
                        }
                    }

                    threadList.add(newT);
                    newT.start();
                    newT = new DeIdentificationThread(namedEntityRecognition);
                }
            }

            // process left-overs
            if (newT.getRecordList().size() > 0) {

                threadList.add(newT);
                newT.start();
            }

            // wait for processing to finish
            try {
                for (DeIdentificationThread t : threadList) {
                    t.join();
                    if (opts.isUpdateOnly()) {
                        connection.updateDeidRecord(opts.getUpdateQuery(), t.getRecordList());
                    } else {
                        connection.insertDeidRecord(opts.getDeidnotestablename(), t.getRecordList());
                    }
                    //connection.insertDeidRecord(opts.getDeidnotestablename(), t.getRecordList());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }

            results.close();
        } catch (SQLException e) {
            System.out.println("Could not execute query: " + opts.getQuery());
            e.printStackTrace();
        }

        try {
            connection.close();
        } catch (Exception e) {
            System.out.println("2: Could not connect to the database dburl: " + opts.getDburl()
                    + ", login: " + opts.getLogin() + ", password: " + opts.getPassword());
            e.printStackTrace();
        }

        java.util.Date endDate = new java.util.Date();
        System.out.println(SimpleIRUtilities.getElapsedTime(startDate, endDate));
    }

    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return encoding.decode(ByteBuffer.wrap(encoded)).toString();
    }

    static String[] loadFileList(File file) throws IOException {

        List<String> list = new ArrayList<String>();
        BufferedReader in = new BufferedReader(new FileReader(file));
        String str;
        while ((str = in.readLine()) != null) {
            String s = str.trim();
            if (s.length() > 0) {
                s = str.toLowerCase(); // normalize to upper case
                list.add(s);
            }
        }
        return list.toArray(new String[list.size()]);
    }
}
